﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnkLASS
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene();
            henn.Nimi = "Henn Sarv";
            henn.Vanus = 64;

            // teine süntaks samale asjale:
            Inimene marju = new Inimene
            {
                Nimi = "Marju Uusen",
                Vanus = 32
            };

            henn.Vanus++;

            List<Inimene> inimesed = new List<Inimene>
            {
                henn, marju,
                new Inimene{Nimi = "Ants", Vanus = 40},
            };

            //foreach (var x in inimesed)
            //{
            //    Console.WriteLine(x);
            //}


            Console.WriteLine(henn);
            Console.WriteLine(henn.Nimi);

            Inimene2 henn2 = new Inimene2
            {
                Eesnimi = "henn2",
                Perenimi = "sarv2",
                //Sünniaeg = DateTime.Parse("1955/03/07")
                Sünniaeg = new DateTime(1955, 03, 07)
            };



            Console.WriteLine(henn2.Vanus());

            henn2.Prindi(); // see on meetodi väljakutse

            //funktsiooni poole pöördumisel atakse ette argumendid. 
            //argument on väärtus, mis omistatakse parameetrile
            Console.WriteLine(Liida(4, 7)); // siin väärtustatakse parameetrid järjest
            Console.WriteLine(Liida(y:4, x:7)); // siin väärtustatakse parameetrid nimeliselt (y ja x võivad olla teises järjekorras). tähtsus - kui parameetried on palju või järjekord ei ole meeles. 

            int x = 4;
            Imelik(ref x); // kui ma panen parameetri ette "ref", siis funktsioonile ei anta ainult väärtus, vaid muutuja ise. Funktsioon teeb mis tahab ja annab muutuja tagasi. Saab tagasi muutuja muudetud seisu
            // seda on vaja öelda funktsiooni defineerides kui ka parameetri juurde
            Console.WriteLine(x);

            int yks = 3; int kaks = 4;

            Vahetus(ref yks, ref kaks);
            Console.WriteLine($"uks: {yks}, kaks: {kaks}");
            

        }

        //teeme klassi "Program" sisse veel mõne meetodi/funktsiooni
        //1. loome funktsiooni, mis liidab kaks arvu kokku ja annab nende summa

        static int Liida(int x, int y) // kaks parameetrit x & y - parameeter on muutuja, mis saab väärtuse funktsiooni väljakutsumise hetkel
            //parameetritel on andmetüüp
        {
            return x + y; // see on funktsiooni body/keha
        }

        //nüüd on nähtus, kus on kaks ühenimelist funktsiooni, aga nad erinevad millegi poolest. Seda nimetatakse:
        //OVERLOAD-imiseks.    OVERLOADED FUNKTSIOON. 
        //võib olla erinev arv parameetried või eri tüüpi parameetrid. funktsiooni parameetreid + funktsiooni nime nimetatakse signatuuriks. funktsioonil peab olema unikaalne signatuur. 
        static int Liida(int x, int y, int z = 7) // optional parameetrite järel ei või olla teist tavalist parameetrit. võib olla optional parameeter ainult
        {
            return x + Liida(y, z);
        }

        static int Liida(short x, int t)
        {
            return x + t;
        }

        static int Summa(int x) => x;
        //static int Summa(int x, int y) => x + y;
        //static int Summa(int x, int y, int z) => x + y + z;

        static int Summa(params int[] arvud) // võtmesõna params tähendab, et on muutuv hulk parameetreid, võivad olla eraldi komadega nimetatud või antud massiivina. See aitab vältida mõttetuid overloadimisi
            //parameter arrayd võib olla ainult 1 parameetrite hulgas ja ta peab olema viimane
        {
            int summa = 0; foreach (var x in arvud) summa += x;
            return summa;
        }

        static void Imelik(ref int miskiArv)
        {
            miskiArv += 7;
        }

        static void Vahetus(ref int arv1, ref int arv2) // see refin värk enamikes keeltes nii ei toimi (õpin seda c-s) 
        {
            int ajutine = arv1;
            arv1 = arv2;
            arv2 = ajutine;
        }

        static void Swap<T>(ref T yks, ref T teine) // T on tüübiparameetier ja selle asja animi on generic funktsioon. nagu list ja dictionary on generic collectonid
        {
            T ajutine = yks;
            yks = teine;
            teine = ajutine;
        }

        //klassikaline rekursiivne funktsioon. funktsioon pöördub enda poole. seda kasutatakse nt mergesort funktsioonis
        static int Factorial(int x)
        {
            if (x < 2) return x;
            return x * Factorial(x - 1);
        }

    }

    //KLASSI TEOORIA, ALL KLASSID DEKLAREERITUD
    // C# saab kirjutada klassi lihtsalt programmi faili sisse. Javas peab lisama iga klassi eraldi failis ja faili nimi peab olema sama, mis klassi nimi
    // C# võib ka klassi jaosk eraldi faili teha, aga programmi tekst määrab klassi nime, mitte faili nimi. siin võib ka osa klassi kirjeldada ühes failis ja osa teises kohas
    //C# keeles ei oma tähtsust, mis järjekorras on klassid defineeritud (faili lõpus olevat klassi saab kasutada programmis faili alguses). 
    //Samuti ei oma tähtsust, mis järjekorras on alamdefinitsioonid klassi definitsiooni sees (nt võib panna vanus ja nimi teistpidi ja klassisisese meetodi kirjelduse võib panna muude kohta

    // üldiselt on klass ja struct suht samad, aga vahe on:
    // klass on reference tüüpi, e klass on pointer, kus on aadress tegelikult kohale mälus, kus on andmed. Sellepärast, kui omistan kahele klassi muutujale sama objekti (aadressi), siis kaks muutujat käsitlevad sedasama tegelikku mälu
    //struct on value tüüpi, structi muutuja sees on nimi ja vanus kohe, ei ole pointer.Kui teha omistamine, siis koperitakse teise muutujasse sisu, 
    //seega on olemas kaks erinevat muutujat, mis on ühesugused aga paiknevad mälus erinevates kohtades

    // klass on definitsioon (Inimene), objekt on konkreetne element, mis vastab sellele definitsioonile (henn)


    //klassis kõigil muutujatel, mis on seal sees kirjeldatud, on vaikimisi väärtus, mis on selle muutuja tüüpi null (int või string või decimal jne). 
    //St kui objekt on deklareeritud, siis kõigil selle klassi antud muutujatel on väärtus olemas (null või antud)


    // juhul kui on klassid erinevates namespaceides, siis tuleb panna üles using "namespace nimi". 

        // FUNKTSIOON VS MEETOD
        //funktsiooni neist saab kasutada avaldistes, meetodit millegi ära tegemiseks
        // st funktsioon lõppeb returniga
        // nad mõlemad on programmiblokid, paiknevad alati klassi definitsiooni sees ja käivad selle klassi juurde

        // public tähendab, et kõik saavad kasutada

    class Inimene
    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"Inimene {Nimi}, kes on {Vanus} aastane"; // nii oskab klass Inimene ennast stringiks teha, selle asja nimi on meetod ehk funktsioon    
    }

    class Inimene2
    {
        string _Eesnimi; // see ei ole public, seda väljaspoole ei näe, aga sellele pääseb ligi läbu funktsiooni eesnimi ja saab muuta meetodiga eesnimi
        string _Perenimi;
        public DateTime Sünniaeg;

        public string GetEesnimi() => _Eesnimi;
        public void SetEesnimi(string nimi) => _Eesnimi = TitleCase(nimi);
        // loon meetodi Inimene2 klassi
        public void Prindi() => // võib ära jätta loogelised sulud, siis panna võrdus ja nool ja funktsiooni puhul jätta ära ka sõna return
        
            Console.WriteLine($"On inimene, kelle nimi on {_Eesnimi} {Perenimi}");

        //read 96-97 ja 105-109 on pmst seesama, aga 17-18 kasutamise puhul tuleb alati kasutada GetEesnimi() ja SetEesnimi, ei saa kasutada Eesnimi

        public string Eesnimi //property
        {
            get { return _Eesnimi; } // getterfunktsioon, kui kasutada välja sisu, nt trükkides. See on funktsioon, mis selle välja väärtus NÄITAB 
            set { _Eesnimi = TitleCase(value); } //setterfunktsioon - meetod, mis meie kontrolli all lubab sellele väljale uusi väärtusi anda
        }

        //veel üks näide, kuidas kasutada saab seda get ja set asja
        private decimal palk;
        public decimal Palk
        {
            get => palk;
            //set => palk = value > palk ? value : palk; // see on property, mis laseb sisestada ainult suuremat numbrit, kui juba on (value on uus väärtus, palk on vana väärtus)
            set // eelmine rida sama lühemalt, see rida sama pikemalt. Settermeetod on see igal juhul
            {
                if (value > palk)
                {
                    palk = value;
                }
            }
        }

        public string Perenimi
        {
            get { return _Perenimi; } // getterfunktsioon
            set { _Perenimi = TitleCase(value); } //settermeetod
        }

        //loon funktsiooni Inimene2 klassi
        public int Vanus() // see on funktsioon (on sulud)
        {
            return (DateTime.Today - Sünniaeg).Days * 4 / 1461; // 4* nelja aasta päevad on selleks, et arvestada liigaastatega
        }

        public int Vanus2 // see on property (ei ole sulge)
        {
            get => (DateTime.Today - Sünniaeg).Days * 4 / 1461;
        }

        public string TäisNimi => _Eesnimi + " " + _Perenimi; // see on read only property (muuta ei saa, ei ole set meetodit


        public static string TitleCase(string nimi)
        {
            string[] nimed = nimi.Split(' ');
            for (int i = 0; i < nimed.Length; i++)
            {
                nimed[i] = nimed[i].Length == 0 ? "" : // kui string on null pikkusega, siis anname vastuseks ka null pikkusega stringi, muul juhul läheb edasi
                nimed[i] = nimed[i].Substring(0, 1).ToUpper() + nimed[i].Substring(1).ToLower();
            }
            return string.Join(" ", nimed);
        }
    }

    
}
